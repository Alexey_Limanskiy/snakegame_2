// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interectable.h"
#include "Bonus.generated.h"

UCLASS()
class SNAKE_API ABonus : public AActor, public IInterectable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonus();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float SpeedValue;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interect(AActor* Interactor, bool bIsHead) override;
};
