// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockAll.h"
#include "SnakeBase.h"

// Sets default values
ABlockAll::ABlockAll()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABlockAll::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlockAll::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlockAll::Interect(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{

		auto Snake = Cast <ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();

			Destroy();	
					
			
		}
	}
}
